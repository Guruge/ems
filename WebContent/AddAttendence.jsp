<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.oop.model.Leave"%>
<%@page import="com.oop.model.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.oop.service.LeaveServiceImpl"%>
<%@page import="com.oop.service.ILeaveService"%>
<%@page import="com.oop.service.EmployeeServiceImpl"%>
<%@page import="com.oop.service.IEmployeeService"%>
<%@page import="com.oop.model.LoginHandler"%>
<%@page import="com.oop.service.LoginServiceImpl"%>
<%@page import="com.oop.service.ILoginService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css" />
<link rel="stylesheet" type="text/css" href="styles/styleLE.css" />
<link rel="stylesheet" type="text/css" href="styles/styleAE.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
	
	
	
	
	  <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style>
	.mtop{
	margin-top:12px !important; 
	}
	
	
	    /* layout.css Style */
.upload-drop-zone {
  height: 200px;
  border-width: 2px;
  margin-bottom: 20px;
}

/* skin.css Style*/
.upload-drop-zone {
  color: #ccc;
  border-style: dashed;
  border-color: #ccc;
  line-height: 200px;
  text-align: center
}
.upload-drop-zone.drop {
  color: #222;
  border-color: #222;
}
	
</style>
</head>

<%    String CurrentDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date() );
 %>

<body>
	<div class="d-flex">
		<jsp:include page="Sidebar.jsp"></jsp:include>
		<!---Page-Content--------------------------------------------------------------------------------------------------------->

		<div id="page-wrapper">
			<div class="container-flex">
				<div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
					<h3>
						<strong><i class="fa fa-list-alt" aria-hidden="true"></i>
							<strong>Upload Files</strong> <small>Please Upload CSV File</small></strong>
					</h3>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 bottomCont" style="padding-bottom:20px">
<!-- ---------------------------------------------------------------------------------------------------------------- -->
<!------ Include the above in your HEAD tag ---------->
					<div class="container-fluid">
					    
					        <div class="panel-heading"></div>
					        <div class="panel-body">
					          <!-- Standar Form -->
					          <h4>Select files from your computer</h4>
					          <form action="AddAttendance" method="post" enctype="multipart/form-data" id="js-upload-form">
					            <div class="form-inline">
					              <div class="form-group">
					                <input type="file" name="file" id="fileUpload"  multiple>  <!--js-upload-files -->
					              </div>
					              <button type="submit" class="btn btn-sm btn-primary"    id="upload" value="Upload">Upload files</button> <!--js-upload-submit -->
					            </div>
					          </form>
				
					        </div>
					     
					    </div> <!-- /container -->

    <!-- --------------------------------------------------------------------------------------------------------- -->
  <!--   <input type="file" id="fileUpload" />
<input type="button" id="upload" value="Upload" />-->
<hr />
<div id="dvCSV" class="container">
</div>
					
				</div>
<!-- ------------------------------------------------------------------------------------------------------------------------------------ -->
			
 
			</div>
		</div>
	</div>
	<script>
    
        
        
    function sidebarToggle() {
  document.getElementById("sidebar-wrapper").classList.toggle("active");
  document.getElementById("page-wrapper").classList.toggle("active");
}
        
    function employeeDropdown(){
        document.getElementById("employeeSubmenu").classList.toggle("active");
    }
        
    function searchbarToggle(){
        document.getElementById("searchbarCollapse").classList.toggle("active");
    }
    
    
    
    
    
    
    
    

	 
	

    
    
    


$(function () {
	$('input[type=file]').change(function() {

        var dataArray = [];
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
        if (regex.test($("#fileUpload").val().toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var table = $("<table />").addClass("table table-striped");;
                    var rows = e.target.result.split("\n");
                    
                    table.append('<thead><tr><th>User ID</th><th>Name</th><th>Date</th><th>Time</th></tr></thead>')
                   
                    for (var i = 1; i < rows.length; i++) {
                        var cells = rows[i].split(",");
                        if( !(cells[1] == "0")  ){
                            var dataRow = { ID :  Number(cells[1]) ,  Name: cells[0] , Date : cells[3] , Time : cells[4] };
                             dataArray.push(dataRow);                     
                        }                       
                    }
                    //display-------------------------------
                    dataArray.forEach( item => {
                    console.log(item);
                    var row = $("<tr />");
                       
                       var cell = $("<td />");
                       cell.html( item.ID  );
                       row.append(cell);

                       var cell = $("<td />");
                       cell.html( item.Name  );
                       row.append(cell);

                       var cell = $("<td />");
                       cell.html( item.Date  );
                       row.append(cell);
                       
                       var cell = $("<td />");
                       cell.html( item.Time  );
                       row.append(cell);

                   table.append(row);
                   
                } );

                    
         $("#dvCSV").html('');
         $("#dvCSV").append(table);

                }
                reader.readAsText($("#fileUpload")[0].files[0]);
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid CSV file.");
        }
    
    //-----------------------------------------------------Display Data-------------------
    
    
    
    });
});

// upload js


+ function($) {
    'use strict';

    // UPLOAD CLASS DEFINITION
    // ======================

    var dropZone = document.getElementById('drop-zone');
    var uploadForm = document.getElementById('js-upload-form');

    var startUpload = function(files) {
        console.log(files)
    }

    uploadForm.addEventListener('submit', function(e) {
        var uploadFiles = document.getElementById('js-upload-files').files;
        e.preventDefault()

        startUpload(uploadFiles)
    })

    dropZone.ondrop = function(e) {
        e.preventDefault();
        this.className = 'upload-drop-zone';

        startUpload(e.dataTransfer.files)
    }

    dropZone.ondragover = function() {
        this.className = 'upload-drop-zone drop';
        return false;
    }

    dropZone.ondragleave = function() {
        this.className = 'upload-drop-zone';
        return false;
    }

}(jQuery);
</script> 
    

</body>
</html>
