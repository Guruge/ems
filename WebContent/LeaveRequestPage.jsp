<%@page import="com.oop.service.LeaveServiceImpl"%>
<%@page import="com.oop.service.ILeaveService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.oop.model.*"%>
<%@page import="com.oop.service.EmployeeServiceImpl"%>
<%@page import="com.oop.service.IEmployeeService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.oop.util.CommonUtil"%>

<%


	LoginHandler login = (LoginHandler) session.getAttribute("loginDetails");
    String CurrentDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date() );
    
    
    
    
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>



<script src="javascript/moment.js"></script>

<link rel="stylesheet" type="text/css" href="styles/style.css" />
<link rel="stylesheet" type="text/css" href="styles/styleLE.css" />
<link rel="stylesheet" type="text/css" href="styles/styleAE.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<style>
.mtop {
	margin-top: 12px !important;
}
.invalidDateSelectionError{
	color:red;
	font-size: 12px;
	font-weight:700;
}
</style>
</head>
<body>
	<div class="d-flex">
		<jsp:include page="Sidebar.jsp"></jsp:include>
		<!---Page-Content--------------------------------------------------------------------------------------------------------->
		<div id="page-wrapper">
			<div class="container-flex">
				<!--Main-Title-of-the-Page-------------------------------------------------->
				<div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
					<h3>
						<strong> <!--Main-Header icon and Text goes here----------------------------->
							<i class="fas fa-award"></i> Leave Management
						</strong>
					</h3>
				</div>
				<!--------------------------------------------------------------------------->
				<!--Form Sides start from here----------------------------------------------->
				<%
					IEmployeeService employeeService = new EmployeeServiceImpl();
					Employee employee = employeeService.getEmployeeDetails(login.getEmpId());
					ILeaveService leavesService =new LeaveServiceImpl();
					Leave leave = leavesService.getavailableLeaves(login.getEmpId());
					ArrayList<Leave> arrayList = leavesService.getHolydates("All");
			%>
								<% int counts = 0;
									List<String> HolidayList = new ArrayList<String>();
							     	int arraySize = 0;
									ILeaveService leaveService = new LeaveServiceImpl();
									ArrayList<Leave> arrayList3 = leaveService.getHolidays("All");
									if(!arrayList.isEmpty() ){
									 for( Leave leaveu : arrayList){  
										 HolidayList.add(leaveu.getHolidayDate());
										 %>
								<%
									}
									} else {
								%>
								
								<% }
									System.out.println("Holiday List :  -");
									System.out.println(HolidayList);
									
									
									
									Leave l = leavesService.getL(login.getEmpId());
				
									%>
	
					 <input type="hidden" 	class="form-control"  name="holidays" id="holidays" value="<%=HolidayList %>" readonly>

				<div class="col-md-12 col-sm-12 col-xs-12 FormSides">

					<!--Left Part of the page Content----------------------------------->
					<div class="col-md-8 col-sm-12 col-xs-12" id="l">
						<div class="col-md-12 col-sm-12 col-xs-12 " id="left">

							<h5 class="FormHeader">
								<strong>Leave Request</strong>
							</h5>
							<!----Input Component---------->

							<form method="post" action="LeaveRequest"  id="LeaveRequestForm" onsubmit="return checkDateRange()">
								<div class="col-md-3">
									<label>Employee ID</label> <input type="text"
										class="form-control" name="EmpId"
										value="<%=login.getEmpId()%>" readonly>
									
								</div>
								<div class="col-md-5">
									<label>Employee Name</label> <input type="text"
										class="form-control" name="EmpName"
										value="<%=login.getEmpName()%>" readonly>
								</div>
								<div class="col-md-4">
									<label>Department</label> <input type="text"
										class="form-control" name="Department"
										value="<%=employee.getDepartmentID()%>" readonly>
								</div>
								
								
								<div class="col-md-5">
									<label>Start Date </label> <input type="date"
										class="form-control" name="StartDate" id="StartDate"
										onchange="dateDiffInDays()" min="<%=CurrentDate%>" required>
								</div>
								<div class="col-md-5">
									<label>End Date</label> <input type="date" class="form-control"
										name="EndDate" id="EndDate" onchange="dateDiffInDays()"
										min="<%=CurrentDate%>" required>
								</div>
								<div class="col-md-2">
									<label>Date Count</label> <input type="text"
										class="form-control" id="count" name="count" readonly
										value="0">
									</td>
								</div>
								<div class="col-md-12" >
								<p class="invalidDateSelectionError" id="invalidDate"> </p>
								</div>

								<div class="col-md-12">
									<div class="col-md-12" style="padding: 0px">
										<label>Leave Type</label>
									</div>
									<div class="col-md-4">
										<input type="radio" name="leaveType" value="Casual" checked
											class="casual"> <span class="casuallable"><b>
												Casual </b></span>
									</div>
									<div class="col-md-4">
										<input type="radio" name="leaveType" value="Annual"
											class="annual"> <span class="annuallable"> <b>
												Annual </b></span>
									</div>
									<div class="col-md-4">
										<input type="radio" name="leaveType" class="sick" value="Sick">
										<span class="sicklable"><b> Sick </b></span>

									</div>
								</div>

								<!--Available   leave Table -->
								<div class="col-xs-12">
									<label>Available Leaves</label>
									<table name="availableLeavesTable">
										<thead>
											<tr>
												<th scope="col">Casual Leaves</th>
												<th scope="col">Annual Leaves</th>
												<th scope="col">Sick Leaves</th>
														<th><p class="invalidDateSelectionError" id="leavecoyuntMinus"> </p></th>
		
											</tr>
										</thead>
										<tbody>

											<tr>
												<td><input type="text"
													class="form-control  tableInputLeaves" id="casualleaves"
													name="casualleaves" readonly
													value="<%=7 - l.getCasualLeaves() %>"></td>
												<td><input type="text"
													class="form-control tableInputLeaves" id="requestleaves"
													name="requestleaves" readonly
													value="<%=14 - l.getRequestLeaves() %>"></td>
												<td><input type="text"
													class="form-control tableInputLeaves" id="sickLeaves"
													name="sickLeaves" readonly
													value="<%=7 - l.getSickLeaves() %>"></td>
													
													
											</tr>
										</tbody>
									</table>
								</div>
								<!---Component End------------->
								<div class="col-md-12" >
									<p class="invalidDateSelectionError" id="noavailableLeaves"> </p>
								</div>
								
								<div class="col-md-12">
									<label>Description</label>
									<textarea class="form-control" rows="10" id="comment"
										name="Description" required></textarea>
								</div>
							
								<!---Component End------------->
								<div class="col-md-12 text-right">
									<br>
									<button type="submit" class="btn btn-sm btn-primary" id="suBF">
										<b>Compose</b>
									</button>
									<button type="cancel" class="btn btn-sm btn-danger">
										<b>Cancel</b>
									</button>
								</div>
							</form>
						</div>
					</div>
					<!--End of the left Part-------------------------------------------->
					<!--Right Part of the page Content----------------------------------->
					<div class="col-md-4 col-sm-12 col-xs-12" id="l">
						<div class="col-md-12 col-sm-12 col-xs-12  " id="right">

							<h5 class="FormHeader">
								<strong>Policy</strong>
							</h5>
							<div class="col-md-12  text-center"></div>
							<div class="col-md-12 ">
								<h6 class="policyTitle">
									<b>MAXIMUM LENGTH OF LEAVE</b>
								</h6>
								<p class="policyDescription">The maximum length of leave
									allowed is 5 days. If the employee needs a longer leave due to
									medical complications, the employee should notify COMPANY as
									soon as possible.</p>

								<h6 class="policyTitle">
									<b>TRANSFERS</b>
								</h6>
								<p class="policyDescription">An employee requesting
									pregnancy leave may also ask for a transfer to another less
									strenuous or less hazardous position if so desired. The request
									must be in writing and must state the reason for the transfer</p>

								<h6 class="policyTitle">
									<b>MEDICAL INCAPACITY </b>
								</h6>
								<p class="policyDescription">The employee may continue to
									work up to the delivery date, depending upon the employee's
									medical circumstances and the nature of the employee's job.</p>
							</div>
						</div>
					</div>

					<!--End of the Right Part-------------------------------------------->
				</div>
			</div>
			<!--End of the Page Content-------------------------------------------->
		</div>
		<script src="javascript/availableLeavesCal.js"></script>
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
			function sidebarToggle() {
				document.getElementById("sidebar-wrapper").classList
						.toggle("active");
				document.getElementById("page-wrapper").classList
						.toggle("active");
			}

			function employeeDropdown() {
				document.getElementById("employeeSubmenu").classList
						.toggle("active");
			}

			function searchbarToggle() {
				document.getElementById("searchbarCollapse").classList
						.toggle("active");
			}
			
			const _MS_PER_DAY = 1000 * 60 * 60 * 24;
			
			
			function dateDiffInDays() {
			const a = new Date(document.getElementById("StartDate").value);
			const b = new Date(document.getElementById("EndDate").value);  
			const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
			const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
			const diff = Math.floor((utc2 - utc1) / _MS_PER_DAY) + 1;
			var itr = moment.twix(new Date(document.getElementById("StartDate").value),new Date(document.getElementById("EndDate").value)).iterate("days");
			var range=[];
			while(itr.hasNext()){
			    range.push(itr.next().format("YYYY-M-D"))
				//console.log("Range Added");
			}
			  var holidays = [];
			  holidays = document.getElementById("holidays").value.replace("[", "").replace("]", "").split(", "); 	 
		     	//console.log(range);
				//console.log(holidays);

				var c = 0;
				
				for (i = 0; i < range.length; i++ ){
					
					var _date = range[i].split("-");
					var d = new Date(_date[0],_date[1], _date[2]);
					//console.log(" the day is : " + d.getDay()+ "   Date is "+_date);
					
					if(d.getDay() == 2 || d.getDay() == 3){
						c++;
					}
						for(j = 0; j < holidays.length; j++){
							
							if(holidays[j] == range[i] ){
								 c++;
								 console.log("Match : " +range[i]);
								 
							}
							
							else
							{
								 console.log("Not Match");
							}
						}
				}

				var tot = 0;
				if(c > 0){
					
					document.getElementById("invalidDate").innerText = " You selected public holidays so those days deduct your data count";
					
				}else{
					document.getElementById("invalidDate").innerText = "";
				}
				if(diff >= 0 && diff != null ) {
					tot =  diff - c
				
					document.getElementById("count").value =  tot; 
				}
			}

			
			$('#EndDate').change(function() {
				
				var availableCasual =  $('#casualleaves').val();
				var availableAnnual = $('#requestleaves').val();
				var availableSick   =  $('#sickLeaves').val();
				var leaveType = 		 $('#leaveType').value;
				
				
				if($("input[type='radio']").is(':checked')) {
				    var leave_type = $("input[type='radio']:checked").val();
				   
				}
			
			    console.log("Available Casual Leaves : " +availableCasual);
				console.log("Available Annual Leaves : " +availableAnnual);
				console.log("Available Sick Leaves : " +availableSick); 
				console.log("Leave type ///// : " +leave_type); 
				 $(".annuallable").css('opacity', '.2'); 
				 $(".sicklable").css('opacity', '.2'); 
				
				 $(".sick").attr('disabled', true);
				 $(".annual").attr('disabled', true);
					  var numberofdates =  $('#count').val();
	
					 console.log("Numberof dates : " +numberofdates);
					  
					  
				    if (numberofdates >= 3) {
				      $(".casual").attr('disabled', false);
				      $(".casuallable").css('opacity', '1'); 

					  
				      $(".annual").attr('disabled', false);
				   	  $(".annuallable").css('opacity', '1');
				      
				      
				      $(".sick").attr('disabled', true);
				      $(".sicklable").css('opacity', '.2');
				    }
				    
				    
				    
				    else if(numberofdates ==1){
				    	 $(".sick").attr('disabled', false);
				    	 $(".sicklable").css('opacity', '1');
				    	 
				    	 
				    	 $(".casuallable").css('opacity', '1');
				    	 $(".casual").attr('disabled', false);

				    	 
				    	 $(".annual").attr('disabled', true);
				    	 $(".annuallable").css('opacity', '0.2');
				    }

				    
				    else {

				    	 $(".casual").attr('disabled', false);
				    	 $(".casuallable").css('opacity', '1')
				    	

				    	 $(".annual").attr('disabled', true);
				    	 $(".annual").css('opacity', '1');
				    	 
				    	 
				    	 $(".sick").attr('disabled', true);
				    	 $(".sicklable").css('opacity', '0.2');

				    }
				    

				    if(availableCasual == 0 && leave_type == "Casual"){
						
						document.getElementById("noavailableLeaves").innerText = " Your available leaves are 0 so your leave deducted from your annual leaves";
						
						$("input[type='radio']").val('Annual');

						
				    }
				    if(availableAnnual < 0){
						document.getElementById("leavecoyuntMinus").innerText = " Your Leaves are less  * ";

				    }

			
			});

			
			//check date range 
			function checkDateRange() {
				
				
			var startDate =  document.getElementById("StartDate").value;
			var endDate =  document.getElementById("EndDate").value;
			
			var availableCasual = document.getElementById("casualleaves").value;
			var availableAnnual = document.getElementById("requestleaves").value;
			var availableSick   = document.getElementById("sickLeaves").value;
			
			var Error = 0;
			
			
			var datecount = document.getElementById("count").value;
			var leaveType = document.querySelector('input[name="leaveType"]:checked').value;
			var sickAvailableLeave = document.getElementById("sickLeaves").value;
			
			

			console.log("check date available  ///// : " +leaveType); 

			
				if(startDate > endDate ){
					document.getElementById("invalidDate").innerText = " Invalid Date Selection";
					Error++;
				}
				else if(datecount < 0 || datecount == 0){
					document.getElementById("invalidDate").innerText = " Your date count invalid";
					Error++;
				}
				
				else if(sickAvailableLeave == 0 && leaveType == "Sick"){
					console.log(" fff"); 

					document.getElementById("noavailableLeaves").innerText = " You unable to get Sick leave because your sick leaves are zero";
					Error++;
				}
				
				
				
				
				if(Error > 0){
					return false;
				}
				else{
					return true;
				}
				 
			}

		</script>
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>

</html>