<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.oop.model.Leave"%>
<%@page import="com.oop.model.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.oop.service.LeaveServiceImpl"%>
<%@page import="com.oop.service.ILeaveService"%>
<%@page import="com.oop.service.EmployeeServiceImpl"%>
<%@page import="com.oop.service.IEmployeeService"%>
<%@page import="com.oop.model.LoginHandler"%>
<%@page import="com.oop.service.LoginServiceImpl"%>
<%@page import="com.oop.service.ILoginService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css" />
<link rel="stylesheet" type="text/css" href="styles/styleLE.css" />
<link rel="stylesheet" type="text/css" href="styles/styleAE.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">

<style>
	.mtop{
	margin-top:12px !important; 
	}
	
</style>
</head>

<%    String CurrentDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date() );
 %>

<body>
	<div class="d-flex">
		<jsp:include page="Sidebar.jsp"></jsp:include>
		<!---Page-Content--------------------------------------------------------------------------------------------------------->

		<div id="page-wrapper">
			<div class="container-flex">



				<div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
					<h3>
						<strong><i class="fa fa-list-alt" aria-hidden="true"></i>
							Add Holidays</strong>
					</h3>

				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 bottomCont" style="padding-bottom:20px">


				<form  method="post" action="AddHolidays" name="addHolidays" >
				
									<div class="col-md-12 field_wrapper"  style="padding : 0px !important">
								    <div class="col-xs-5">
									<label>Date <span id="dateError" class="spanError"></span></label> <input type="date"
										class="form-control"  id="dateOfLeave"  min="<%=CurrentDate%>" required >
									</div>
								    <div class="col-sm-3 col-xs-5" >
									<label>Holiday Type</label>
											 <input type="text"
										class="form-control" id="leaveTypeSelector"  name="leaveTypeSelector"
										 required>
									</div>
									 <div class="col-xs-2" >
									 	<button type="button" class="btn btn-sm btn-info add_button" style="margin-top : 25px" > <b>Add</b> </button>
									 </div>
								</div>
									  <div class="col-md-12 col-sm-12 col-xs-12" >
						                    <div class="col-md-7" > <h5> </h5></div>
						                    <div class="col-md-5 text-right" style="padding-top : 10px;">
						                    <button class= "btn btn-primary ">Submit Form</button>
						                     <button type="reset" class= "btn  btn-danger ">Cancel</button>
						                         </div>
						               </div>
					</form>
					
				</div>
<!-- ------------------------------------------------------------------------------------------------------------------------------------ -->
			
 
			</div>
		</div>
	</div>
	<script>
    
        
        
    function sidebarToggle() {
  document.getElementById("sidebar-wrapper").classList.toggle("active");
  document.getElementById("page-wrapper").classList.toggle("active");
}
        
    function employeeDropdown(){
        document.getElementById("employeeSubmenu").classList.toggle("active");
    }
        
    function searchbarToggle(){
        document.getElementById("searchbarCollapse").classList.toggle("active");
    }
    
    
    
    
    
    
    
    
    
    
	$(document).ready(function(){
	    var maxField = 5; //Input fields increment limitation
	    var addButton = $('.add_button'); //Add button selector
	    
	    var dateInput = $('#dateOfLeave');
	    var leaveSelector = $('#leaveTypeSelector');
	    
	    var wrapper = $('.field_wrapper'); //Input field wrapper
	    var fieldHTML = '';
	    
	    var dateOfLeave = '';
	    var leaveType = '';
	    var datearray = [];
	    
	    
	    
	    var x = 0; //Initial field counter is 1
	    
	    //Once add button is clicked
	    $(addButton).click(function(){
	        //Check maximum number of input fields
	        if(x <= maxField - 1){ 
	        
	            
	            dateOfLeave = dateInput.val();
	            leaveType = leaveSelector.val();
	            
	            if(dateOfLeave == '' || dateOfLeave == null  ){
	            	$('#dateError').text("Date Can't be Empty");
	            
	            }else if(datearray.includes( dateOfLeave ) ){
	            	$('#dateError').text("Date Already Exist");
	            	
	            }else{
	            	
	            datearray.push(dateOfLeave);
	            x++; 
	            
	            fieldHTML += '<div><div  class="col-xs-5 mtop"><input type="text" readonly class="form-control" name="date"  value="'+  dateOfLeave +'" ></div>';
	            fieldHTML += '<div class="col-sm-3 col-xs-5 mtop" ><input type="text" readonly class="form-control" name="type"   value="'+  leaveType +'"  >';
			    fieldHTML += '</div> <div class="col-xs-2 mtop" ><a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm"><b>Remove</b></a></div></div>';
			    
			    $('#dateError').text("");
	            $(wrapper).append(fieldHTML); //Add field html
	            fieldHTML = '';
	            
	            }
	            
	        }
	    });
	    
	    
	    
	    
	    //Once remove button is clicked
	    $(wrapper).on('click', '.remove_button', function(e){
	        e.preventDefault();
	        var value =  $(this).parent('div').parent('div').find('input').first().val(); 
	        datearray = datearray.filter(item => item !== value);
	        $(this).parent('div').parent('div').remove(); //Remove field html
	      
	        x--; //Decrement field counter
	    });
	});
	 
	
    </script>

</body>
</html>
