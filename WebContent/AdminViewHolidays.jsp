<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.oop.model.Leave"%>
<%@page import="com.oop.model.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.oop.service.LeaveServiceImpl"%>
<%@page import="com.oop.service.ILeaveService"%>
<%@page import="com.oop.service.EmployeeServiceImpl"%>
<%@page import="com.oop.service.IEmployeeService"%>
<%@page import="com.oop.model.LoginHandler"%>
<%@page import="com.oop.service.LoginServiceImpl"%>
<%@page import="com.oop.service.ILoginService"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css" />
<link rel="stylesheet" type="text/css" href="styles/styleLE.css" />
<link rel="stylesheet" type="text/css" href="styles/styleAE.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

</head>
<body>
	<div class="d-flex">
		<jsp:include page="Sidebar.jsp"></jsp:include>
		<!---Page-Content--------------------------------------------------------------------------------------------------------->

		<div id="page-wrapper">
			<div class="container-flex">



				<div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
					<h3>
						<strong><i class="fa fa-list-alt" aria-hidden="true"></i>
							View Holidays</strong>
					</h3>

				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 bottomCont">


					<div class="col-md-12 col-sm-12 col-xs-12 table-responsive cont" >
						<table class="table table-striped " id="holidayTable"  style="font-size: 1.1em">
							<thead
								style="background-color: rgba(1, 50, 67, 1); color: white;">
								<tr>
									<th scope="col" width="15%">ID</th>
									<th scope="col" width="40%">Date</th>
									<th scope="col" width="40%">Type</th>
									

								</tr>
							</thead>
							<tbody>
								<% int count = 0;
									
									ILeaveService leaveService = new LeaveServiceImpl();
									ArrayList<Leave> arrayList = leaveService.getHolidays("All");
									if(!arrayList.isEmpty() ){
									
									 for( Leave leave : arrayList){     %>
										
								<tr>
									<td><%=leave.getHolidayId() %></td>
									<td><%=leave.getHolidayDate() %></td>
									<td><%=leave.getHolidayType() %></td>
									
									
								<%
									}
									} else {
								%>
								<tr>
								<td><td><b>No records</b></td></tr>
								<% } %>
							</tbody>
						</table>
					</div>
					
				</div>
			
<!-- ------------------------------------------------------------------------------------------------------------------------------------ -->
				<div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader" style="margin-top:15px;">
				<div class="col-xs-12">
				
						<h6 class="policyTitle">
							<b>MAXIMUM LENGTH OF LEAVE</b>
						</h6>
						<p class="policyDescription">The maximum length of leave
							allowed is 5 days. If the employee needs a longer leave due to
							medical complications, the employee should notify COMPANY as soon
							as possible.</p>

						<h6 class="policyTitle">
							<b>TRANSFERS</b>
						</h6>
						<p class="policyDescription">An employee requesting pregnancy
							leave may also ask for a transfer to another less strenuous or
							less hazardous position if so desired. The request must be in
							writing and must state the reason for the transfer</p>

						<h6 class="policyTitle">
							<b>MEDICAL INCAPACITY </b>
						</h6>
						<p class="policyDescription">The employee may continue to work
							up to the delivery date, depending upon the employee's medical
							circumstances and the nature of the employee's job.</p>
					</div>
					</div>
 
			</div>
		</div>
	</div>
	        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
	
	<script>
	$(document).ready( function () {
        $('#holidayTable').DataTable();
    } );
        
        
    function sidebarToggle() {
  document.getElementById("sidebar-wrapper").classList.toggle("active");
  document.getElementById("page-wrapper").classList.toggle("active");
}
        
    function employeeDropdown(){
        document.getElementById("employeeSubmenu").classList.toggle("active");
    }
        
    function searchbarToggle(){
        document.getElementById("searchbarCollapse").classList.toggle("active");
    }
    </script>

</body>
</html>
