<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.oop.model.*" %>
<%@page import="com.oop.service.EmployeeServiceImpl"%>
<%@page import="com.oop.service.IEmployeeService"%> 
<%@ page import="com.oop.model.LoginHandler" %> 
<%@ page import="com.oop.model.Department" %> 
<%@ page import="com.oop.service.IDepartmentService" %> 
<%@ page import="com.oop.service.DepartmentServiceImpl" %> 
<%@ page import="java.util.ArrayList" %>
<%
		if(session.getAttribute("loginDetails") == null )
		{
       		getServletContext().getRequestDispatcher("/LoginPage.jsp").forward(request, response);
			return;
		} 
      LoginHandler login = (LoginHandler) session.getAttribute("loginDetails"); %> 
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/style.css" />
    <link rel="stylesheet" type="text/css" href="styles/styleLE.css" />
    <link rel="stylesheet" type="text/css" href="styles/styleAE.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    

</head>
	
<body>
		<% int EmpId = Integer.parseInt(request.getAttribute("EmpId").toString());
		IEmployeeService employeeService = new EmployeeServiceImpl();
			Employee employee = employeeService.getEmployeeDetails(EmpId);
		%>
    <div class = "d-flex">
 	<jsp:include page="Sidebar.jsp"></jsp:include>
     <!---Page-Content--------------------------------------------------------------------------------------------------------->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    
  <div id="page-wrapper" >  
       <div class="container-flex" >
        
                <!--Main-Title-of-the-Page-------------------------------------------------->
                <div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
                    <h3><strong>
                        <!--Main-Header icon and Text goes here----------------------------->
                        <i class="fas fa-address-card"></i>
                       Reset Password</strong></h3>
               </div>
                <!--------------------------------------------------------------------------->
                <!--Form Sides start from here----------------------------------------------->

                <form method = "post" action="ResetPW" onsubmit="return checkpassword()" >
                <div class="col-md-12 col-sm-12 col-xs-12 FormSides">
                   
                      <!--Left Part of the page Content----------------------------------->
                    <div class="col-md-6 col-sm-12 col-xs-12" id="l">

                      
                        <div class="col-md-12 col-sm-12 col-xs-12 " id="left">
                            <!--Header-->
                            <h5 class="FormHeader"><strong>Employee Details</strong></h5>
                            <!----Input Component---------->
                            <div class="col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="fname" readonly value  = "<%=employee.getFirstName() %>" data-validation="required">
                            </div>
                            <!---Component End------------->
                            <div class="col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lname"  readonly value = "<%=employee.getLastName() %>" data-validation="required">
                            </div>
                            <div class="col-md-6">
                                <label>Employee ID</label>
                            <input type="text" class="form-control" name="empid" readonly value="<%=employee.getEmpId()%>" >
                            </div>
                            <div class="col-md-6">
                                 <label>Designation</label>
                               <input type="text" class="form-control" name="designation" readonly value="<%=employee.getDesignation()%>" data-validation="required"> 
                            </div>
                         
                     
                   
                                <!---Component End------------->
                       
                                <!---Component End------------->
                                
                        </div>
                    </div>

            

            
            
                                <div class="col-md-6 col-sm-12 col-xs-12" id="l" styles="padding-bottom : 40px">
                        <div class="col-md-12 col-sm-12 col-xs-12  " id="right">

                            <h5 class="FormHeader"><strong>Password Reset</strong></h5>
								 <!----Input Component---------->
                                 <div class="col-md-6">
                                        <label>New Password</label>
                                        <input type="password" class="form-control" name="newpassword" id="newpassword" value=""  >
                                    </div>
                                    <!---Component End------------->
                             <!----Input Component---------->
                                 <div class="col-md-6">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" name="confirmnewpassword"  id="confirmnewpassword" value= ""  >
                                    </div>
                                    <div class="col-md-12">
                                    <p style="color:red; font-size:12px" id="passwordError"> <b>  </b> </p>
                                    </div>
                                    <!---Component End------------->
                         
                                    <!---Component End------------->
                                   
                                 
                        
                        
                       
                    </div>

                </div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
           <!--End of the Right Part-------------------------------------------->
                     <!--Form-Submit-of-the-Page-------------------------------------------------->
                <div class="col-md-12 col-sm-12 col-xs-12" id="contentHeader">
                    <div class="col-md-7" > <h5> </h5></div>
                    <div class="col-md-5 text-right" style="padding-top : 10px;">
                    <button class= "btn btn-primary ">Update</button>
                     <button class= "btn  btn-danger ">Cancel</button>
                         </div>
               </div>
                    </form>
                <!--------------------------------------------------------------------------->
        </div>
    </div>
     <!--End of the Page Content-------------------------------------------->
  </div>  
    
    <script>  
    
   function checkpassword(){
	   Error = 0;
	   
	   var pas =   document.getElementById("newpassword").value;
	   var conpas =   document.getElementById("confirmnewpassword").value;
	   
	   if(pas == conpas ){
		   document.getElementById("passwordError").innerText = "";
	   }
	   else{
		   Error++;
		   document.getElementById("passwordError").innerText = " * Password didn't match";

	   }
	   
	   
	   if(Error > 0){
		   return false
	   }
	   else{
		   return true;
	   }
   }
    
    
    
    
    function sidebarToggle() {
        document.getElementById("sidebar-wrapper").classList.toggle("active");
        document.getElementById("page-wrapper").classList.toggle("active");
}
        
    function employeeDropdown(){
        document.getElementById("employeeSubmenu").classList.toggle("active");
    }
        
    function searchbarToggle(){
        document.getElementById("searchbarCollapse").classList.toggle("active");
    }
    
    $.validate({
        modules : 'html5'
      });
      $.validate({
    	  modules : 'date'
    	});
    </script>
    
    
    
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

</body>


</html>