package com.oop.model;

import java.util.ArrayList;

public class DailyAttendance {
	
	private String date;
	private ArrayList<AttendanceRow> attendance;

	
	
	
	public DailyAttendance() {
		super();
	}
	public DailyAttendance(String date) {
		super();
		this.date = date;
	}
	public DailyAttendance(String date, ArrayList<AttendanceRow> attendance) {
		super();
		this.date = date;
		this.attendance = attendance;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the attendance
	 */
	public ArrayList<AttendanceRow> getAttendance() {
		return attendance;
	}
	/**
	 * @param attendance the attendance to set
	 */
	public void setAttendance(ArrayList<AttendanceRow> attendance) {
		this.attendance = attendance;
	}
	
	
	
}
