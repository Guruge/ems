package com.oop.model;

public class AttendanceRow {

	private int Empid;
	private String Date;
	private String InTime;
	private String OutTime;
	private  int DepId;
	private int Mark;
	

	

	public AttendanceRow(int empid) {
		super();
		Empid = empid;
	}
	public AttendanceRow(int empid, String date, String inTime, String outTime, int depId, int mark) {
		super();
		Empid = empid;
		Date = date;
		InTime = inTime;
		OutTime = outTime;
		DepId = depId;
		Mark = mark;
	}
	/**
	 * @return the empid
	 */
	public int getEmpid() {
		return Empid;
	}
	/**
	 * @param empid the empid to set
	 */
	public void setEmpid(int empid) {
		Empid = empid;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}
	/**
	 * @return the inTime
	 */
	public String getInTime() {
		return InTime;
	}
	/**
	 * @param inTime the inTime to set
	 */
	public void setInTime(String inTime) {
		InTime = inTime;
	}
	/**
	 * @return the outTime
	 */
	public String getOutTime() {
		return OutTime;
	}
	/**
	 * @param outTime the outTime to set
	 */
	public void setOutTime(String outTime) {
		OutTime = outTime;
	}
	/**
	 * @return the depId
	 */
	public int getDepId() {
		return DepId;
	}
	/**
	 * @param depId the depId to set
	 */
	public void setDepId(int depId) {
		DepId = depId;
	}
	/**
	 * @return the mark
	 */
	public int getMark() {
		return Mark;
	}
	/**
	 * @param mark the mark to set
	 */
	public void setMark(int mark) {
		Mark = mark;
	}
	
	
	
	
	
}
