/**
 * 
 */
package com.oop.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Pubudu Arosha
 *
 */
public class Leave {

	private int LeaveId;
	private int EmpId;
	private String EmployeeName;
	private int DepNo;
	private String StartDate;
	private String EndDate;
	private int count;
	private String Description;
	private String Status;
	private String Feedback;
	private String Type;
	
	
//	leaves
	private int CasualLeaves;
	private int SickLeaves;
	private int RequestLeaves;
	
	
	
	
	//add holidays
	private int HolidayId;
	private String HolidayDate;
	private String HolidayType;
	
	
	
	
	
	
	
	/**
	 * @return the holidayId
	 */
	public int getHolidayId() {
		return HolidayId;
	}
	/**
	 * @param holidayId the holidayId to set
	 */
	public void setHolidayId(int holidayId) {
		HolidayId = holidayId;
	}
	/**
	 * @return the holidayDate
	 */
	public String getHolidayDate() {
		return HolidayDate;
	}
	/**
	 * @param holidayDate the holidayDate to set
	 */
	public void setHolidayDate(String holidayDate) {
		HolidayDate = holidayDate;
	}
	/**
	 * @return the holidayType
	 */
	public String getHolidayType() {
		return HolidayType;
	}
	/**
	 * @param holidayType the holidayType to set
	 */
	public void setHolidayType(String holidayType) {
		HolidayType = holidayType;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return Type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		Type = type;
	}
	/**
	 * @return the empId
	 */
	public int getEmpId() {
		
		return EmpId;
	}
	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(int empId) {
		EmpId = empId;
	}
	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return EmployeeName;
	}
	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}
	/**
	 * @return the depNo
	 */
	public int getDepNo() {
		return DepNo;
	}
	/**
	 * @param depNo the depNo to set
	 */
	public void setDepNo(int depNo) {
		DepNo = depNo;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return StartDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return EndDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the feedback
	 */
	public String getFeedback() {
		return Feedback;
	}
	/**
	 * @param feedback the feedback to set
	 */
	public void setFeedback(String feedback) {
		Feedback = feedback;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	leaves 
	
	
	
	
	
	/**
	 * @return the leaveId
	 */
	public int getLeaveId() {
		return LeaveId;
	}
	/**
	 * @param leaveId the leaveId to set
	 */
	public void setLeaveId(int leaveId) {
		LeaveId = leaveId;
	}
	/**
	 * @return the casualLeaves
	 */
	public int getCasualLeaves() {
		return CasualLeaves;
	}
	/**
	 * @param casualLeaves the casualLeaves to set
	 */
	public void setCasualLeaves(int casualLeaves) {
		CasualLeaves = casualLeaves;
	}
	/**
	 * @return the sickLeaves
	 */
	public int getSickLeaves() {
		return SickLeaves;
	}
	/**
	 * @param sickLeaves the sickLeaves to set
	 */
	public void setSickLeaves(int sickLeaves) {
		SickLeaves = sickLeaves;
	}
	/**
	 * @return the requestLeaves
	 */
	public int getRequestLeaves() {
		return RequestLeaves;
	}
	/**
	 * @param requestLeaves the requestLeaves to set
	 */
	public void setRequestLeaves(int requestLeaves) {
		RequestLeaves = requestLeaves;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
