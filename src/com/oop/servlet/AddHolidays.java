package com.oop.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oop.model.Leave;
import com.oop.service.ILeaveService;
import com.oop.service.LeaveServiceImpl;

/**
 * Servlet implementation class AddHolidays
 */
@WebServlet("/AddHolidays")
public class AddHolidays extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddHolidays() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dates [] = request.getParameterValues("date");
		String types [] = request.getParameterValues("type");
		
		ArrayList<Leave> leaves = new ArrayList<Leave>();
		
		for (int i = 0; i < dates.length; i++) {
			
			Leave leave = new Leave();
			leave.setHolidayDate( dates[i]);
			leave.setHolidayType( types[i] );
			
			System.out.println("Date : " + leave.getHolidayDate() + " Type : " + leave.getHolidayType() );
			leaves.add(leave);
		
			
		}
		
			
			ILeaveService leaveService = new LeaveServiceImpl();
			leaveService.addHolidays(leaves);
			
			RequestDispatcher dispatch  = getServletContext().getRequestDispatcher("/AdminViewHolidays.jsp");
			dispatch.forward(request, response);
	
	}

}
