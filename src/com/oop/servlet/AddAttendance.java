package com.oop.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import com.oop.model.AttendanceRow;
import com.oop.model.DailyAttendance;
import com.oop.service.DailyAttendanceImpl;
import com.oop.service.IDailyAttendanceService;


/**
 * Servlet implementation class AddAttendance
 */
@WebServlet("/AddAttendance")
@MultipartConfig
public class AddAttendance extends HttpServlet {
	
	private String filePath = System.getProperty("catalina.home") + "\\EMS\\WebContent\\files\\";
	private int EmpId;
	private String value;
	private File file ;
	private String fileName;
	private String date = "01/01/1970";
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public AddAttendance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		ServletFileUpload.isMultipartContent(request);
		ArrayList<DailyAttendance> list  = new ArrayList<DailyAttendance>();
		IDailyAttendanceService dailyAttendanceService = new DailyAttendanceImpl();
		DailyAttendance dailyAttendacelist = new DailyAttendance("01/01/1970" );
		SimpleDateFormat timeformatter = new SimpleDateFormat("hh:mm:ss");
		Calendar cal = Calendar.getInstance();
		
		
		try {
		       List<FileItem> fileItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
		    
			DiskFileItemFactory factory = new DiskFileItemFactory();
			   
		      // maximum size that will be stored in memory
		      factory.setSizeThreshold(8000);

		      new ServletFileUpload(factory);
		      
		     
		    	  // Process the uploaded file items
		         Iterator i = fileItems.iterator();

		         
		         while ( i.hasNext () ) {
		            FileItem fi = (FileItem)i.next();
		            if ( fi.isFormField () ) { 
		            	
		             value = fi.getString();
		            	 
		            }
		            else{
		            	
		            	// Get the uploaded file parameters
		            	fileName = fi.getName();
		     
		            
		               // Write the file
		               if( fileName.lastIndexOf("\\") >= 0 ) {
		                  file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
		               } else {
		                  file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
		               }
		               fi.write( file ) ;
		               System.out.println("Uploaded Filename: " + filePath + fileName );
		              
		            }
		         }
		         
		         } catch(Exception ex) {
		            System.out.println(ex);
		         }
		
		
		 String line = "";
	        String cvsSplitBy = ",";
	        dailyAttendacelist = new DailyAttendance( date , dailyAttendanceService.getList() );
	        try (BufferedReader br = new BufferedReader(new FileReader(filePath + fileName))) {
	        int counter = 0;
	            while ((line = br.readLine()) != null) {

	              //----------------------------------------------------------
	                String[] row = line.split(cvsSplitBy);
	                
	                if( counter > 0 ) {
	                
	                int emp = Integer.parseInt( row[1]);
	                
	               if( !row[1].equalsIgnoreCase("0")  ) {
	            	   
	            	  
	            	   try {
						cal.setTime(timeformatter.parse(row[4]));
					} catch (ParseException e) {
						System.out.println(e);
					}
	            	   
	            	   int indexnox = dailyAttendanceService.selectAttendaceObjectIndex( emp , dailyAttendacelist.getAttendance() );
	            	   if(indexnox == -99  ) {
	            		   System.out.println("removed");
	            	   }else {
	            	   
	            	   if( date.equalsIgnoreCase(row[3] )) {
	            		   
	            		   int indexno = dailyAttendanceService.selectAttendaceObjectIndex( emp , dailyAttendacelist.getAttendance() );
	            		   
	            		   //get row 
	            		   AttendanceRow Attendacerow = dailyAttendacelist.getAttendance().get(  indexno  );
	            		   dailyAttendacelist.getAttendance().remove(indexno);
	            		   
	            		   Attendacerow.setDate(row[3]);
	            		   Attendacerow.setMark(1);
	            		   
	            		   if( cal.get(Calendar.HOUR_OF_DAY) >= 15 ) {
	            			   Attendacerow.setOutTime(row[4]);
	            		   }else {
	            			   Attendacerow.setInTime(row[4]);
	            		   }

	            		   dailyAttendacelist.getAttendance().add(Attendacerow);
	            		   
	            		   
	            		   
	            	   }else {
	            		   //add the current daily list to main list
	            		   list.add(dailyAttendacelist);
	            		   
	            		   //create the new day attendace
	            		   dailyAttendacelist = new DailyAttendance( row[3] , dailyAttendanceService.getList() );
	            		   
	            		   int indexno = dailyAttendanceService.selectAttendaceObjectIndex( emp , dailyAttendacelist.getAttendance() );   
	            		   
	            		  //get row 
	            		   AttendanceRow Attendacerow = dailyAttendacelist.getAttendance().get(  indexno  );
	            		   dailyAttendacelist.getAttendance().remove(indexno);
	            		   
	            		   Attendacerow.setDate(row[3]);
	            		   Attendacerow.setMark(1);
	            		   
	            		   if( cal.get(Calendar.HOUR_OF_DAY) >= 15 ) {
	            			   Attendacerow.setOutTime(row[4]);
	            		   }else {
	            			   Attendacerow.setInTime(row[4]);
	            		   }
	            		   
	            		   
	            		   dailyAttendacelist.getAttendance().add(Attendacerow);
	            		   
	            		   date = row[3];
	            		   
	            	   
	            	   }
	            	   
	            	   }
	                System.out.println("User  =  " + row[0] +    " | WorkID =" + row[1]  +    " | Date =" + row[3]+    " | Time =" + row[4]+    " | IN/OUT =" + row[5]);
	            	   
	              }
	               
	               //--------------------------------------------------------
	            }
	                counter++;
//	            	System.out.println("User  =  " + row[0] +    " | WorkID =" + row[1]  +    " | Date =" + row[3]+    " | Time =" + row[4]+    " | IN/OUT =" + row[5]);
//	            	System.out.println("");
	            }
	            
	            System.out.println(counter);
	            list.add(dailyAttendacelist);

	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        
	        try  
	        {         
	        	File f= new File(filePath + fileName);           //file to be delete  
		        if(f.delete()){  
			        	System.out.println(f.getName() + " deleted");   //getting and printing the file name  
			    }else{  
		        	System.out.println("failed");  
		        }  
	        }  
	        catch(Exception e)  {  }
	        
	        
	        list.remove(0);
	        for( DailyAttendance da : list) {
	        	
	        	System.out.println("\n----------------------------------------------");
	        	System.out.println("Date : "+ da.getDate() );
	        	
	        	ArrayList<AttendanceRow> rows = da.getAttendance();
	        	if(! (rows == null) ) {
	        	for(AttendanceRow row : rows ) {
	        		
	        		System.out.println("\n\nEmpID : "+ row.getEmpid() );
	        		System.out.println("Mark : "+ row.getMark() );
	        		System.out.println("InTime : "+ row.getInTime() );
	        		System.out.println("OutTime : "+ row.getOutTime() );
	        	}
	        	}
	        	
	        }
	       
	        
	       dailyAttendanceService.addToDatabase(list);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ListAttendance.jsp");
		dispatcher.forward(request, response);
	}

}
