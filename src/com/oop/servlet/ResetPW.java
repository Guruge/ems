package com.oop.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oop.model.Employee;
import com.oop.service.EmployeeServiceImpl;
import com.oop.service.IEmployeeService;
import com.oop.util.CommonUtil;

/**
 * Servlet implementation class ResetPW
 */
@WebServlet("/ResetPW")
public class ResetPW extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResetPW() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee employee = new Employee();
		IEmployeeService employeeService = new EmployeeServiceImpl();

		
		employee.setEmpId(  Integer.parseInt(request.getParameter("empid")) );
		
				

				
		employee.setPassword((CommonUtil.get_SHA_256_SecurePassword(request.getParameter("newpassword") )));
				
				
				
				
				
		employeeService.resetPassowrd(employee);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ListEmployeePage.jsp");
		dispatcher.forward(request, response);
		
	}

}
