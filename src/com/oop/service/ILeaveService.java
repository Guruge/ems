package com.oop.service;

import java.util.ArrayList;

import com.oop.model.Leave;

public interface ILeaveService {

	//public void addLeave( ArrayList<Leave> leave);
	public void addHolidays(ArrayList<Leave> leave);
	public ArrayList<Leave> getHolidays(String Action);
	public void addLeave(Leave leave);
//	public ArrayList<Leave> getHolidaysForLeaveManagment(String Action);
	
	
	public ArrayList<Leave> getLeavesList(int EmpId);
	public ArrayList<Leave> getHolydates(String Action);
	
	public ArrayList<Leave> getLeavesList(String Action);
	
	public void manageLeave(Leave leave );
	
	public Leave getavailableLeaves(double EmpId);
	
	public Leave getL(int Empid);
}
