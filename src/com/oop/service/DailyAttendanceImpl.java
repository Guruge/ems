package com.oop.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.oop.model.Attendance;
import com.oop.model.AttendanceRow;
import com.oop.model.DailyAttendance;
import com.oop.util.CommonConstants;
import com.oop.util.CommonUtil;
import com.oop.util.DBConnectionUtil;
import com.oop.util.QueryUtil;

public class DailyAttendanceImpl implements IDailyAttendanceService {

	public static Connection connection ;
	public static PreparedStatement preparedStatement ;
	
	@Override
	public ArrayList<AttendanceRow> getList() {
		
		ArrayList<AttendanceRow> arrayList = new ArrayList<>();
		try {
			 
			 
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_ALL_EMPLOYEE_IDS));
			connection.setAutoCommit(false);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while( resultSet.next() )
			{
				AttendanceRow attendance = new AttendanceRow(resultSet.getInt(1));
				attendance.setInTime("0:00:00");
				attendance.setOutTime("0:00:00");
				attendance.setMark(0);
				
				arrayList.add(attendance);
				
			}
			connection.commit();
			
		} catch (SQLException | IOException | ParserConfigurationException | ClassNotFoundException | SAXException  e) {
			System.out.print(e);
		}finally {
			/*
			 * Close prepared statement and database connectivity at the end of transaction
			 */
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.print(e);
			}
		}
		
		return arrayList;
		
	}

	@Override
	public int selectAttendaceObjectIndex(int emp, ArrayList<AttendanceRow> arraylist) {
		int counter = 0;
		if( arraylist != null  ) {
		for ( AttendanceRow attendanceRow : arraylist ) {
			if( attendanceRow.getEmpid() == emp ) {
				return counter;
			}
			
			counter++;
		}
		}
		return -99;
	}

	@Override
	public void addToDatabase(ArrayList<DailyAttendance> arraylist) {
		try {
					
					connection= DBConnectionUtil.getDBConnection();
					preparedStatement = connection.prepareStatement( QueryUtil.queryByID(CommonConstants.ID_ADD_ATTENDANCE ));
					SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd");
					int counter = 1;
					
					connection.setAutoCommit(false);
				
					for(DailyAttendance attendance : arraylist ){
						
						ArrayList<AttendanceRow> rows = attendance.getAttendance();
			        	if(! (rows == null) ) {
			        	for(AttendanceRow row : rows ) {
			        	
			        	System.out.println("counter : " + counter++ );
						preparedStatement.setInt(1,  row.getEmpid() );
						preparedStatement.setString(2, new SimpleDateFormat("yyyy-MM-dd").format( dateformatter.parse( attendance.getDate() ) )  );						
						preparedStatement.setInt(3, row.getMark()  );
						preparedStatement.setString(4, row.getInTime() );
						preparedStatement.setString(5, row.getOutTime()  );
						preparedStatement.addBatch();
			        	  }
			        	}
					}
					preparedStatement.executeBatch();
					connection.commit();
					
				} catch (ClassNotFoundException | SQLException | SAXException | IOException | ParserConfigurationException | ParseException  e) {
					System.out.println(e);
				} finally {
					/*
					 * Close prepared statement and database connectivity at the end of transaction
					 */
					try {
						if (preparedStatement != null) {
							preparedStatement.close();
						}
						if (connection != null) {
							connection.close();
						}
					} catch (SQLException e) {
						System.out.print(e);
					}
				}
		
				
			}

}
