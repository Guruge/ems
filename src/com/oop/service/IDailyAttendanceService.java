package com.oop.service;

import java.util.ArrayList;

import com.oop.model.AttendanceRow;
import com.oop.model.DailyAttendance;

public interface IDailyAttendanceService {
	
	public ArrayList<AttendanceRow> getList();
	
	public int selectAttendaceObjectIndex( int emp  , ArrayList<AttendanceRow> arraylist );
	
	public void addToDatabase( ArrayList<DailyAttendance> arraylist );


}
