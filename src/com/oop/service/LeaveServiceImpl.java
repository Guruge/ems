/**
 * 
 */
package com.oop.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.oop.model.Employee;
import com.oop.model.Leave;
import com.oop.util.CommonConstants;
import com.oop.util.DBConnectionUtil;
import com.oop.util.QueryUtil;

/**
 * @author Pubudu Arosha
 *
 */
public class LeaveServiceImpl implements ILeaveService {

	
	private static Connection connection;
	private static PreparedStatement preparedStatement;
//	private static PreparedStatement preparedStatementLeaveAdd;

	/* (non-Javadoc)
	 * @see com.oop.service.ILeaveService#addLeave(com.oop.model.Leave)
	 */
	@Override
//	public void addLeave(ArrayList<Leave> leaves) {
//		try {
//			connection = DBConnectionUtil.getDBConnection();
//			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_ADD_LEAVE));
//			
//		
//			connection.setAutoCommit(false);
//			
//			for( Leave leave : leaves ) {
//			preparedStatement.setInt( 1, leave.getEmpId() );
//			preparedStatement.setInt( 2, leave.getDepNo() );
//			preparedStatement.setString( 3, leave.getStartDate() );
//			preparedStatement.setString( 4, leave.getType() );
//			preparedStatement.setString( 5, leave.getDescription() );
//			preparedStatement.setString( 6, leave.getStatus() );
//			preparedStatement.addBatch();
//			 
//			
//			
//			}
//			preparedStatement.executeBatch();
//			connection.commit();
//		
//			
//		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
//			System.out.print(e);
//		}	finally {
//			
//			try {
//				if (preparedStatement != null) {
//					preparedStatement.close();
//				}
//				if (connection != null) {
//					connection.close();
//				}
//			} catch (SQLException e) {
//				System.out.print(e);
//			}
//		}
//
//	}
	
	public void addLeave(Leave leave) {
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_ADD_LEAVE));
			connection.setAutoCommit(false);
			preparedStatement.setInt( 1, leave.getEmpId() );
			preparedStatement.setInt( 2, leave.getDepNo() );
			preparedStatement.setString( 3, leave.getStartDate() );
			preparedStatement.setString( 4, leave.getEndDate() );
			preparedStatement.setInt( 5, leave.getCount() );
			preparedStatement.setString( 6, leave.getDescription() );
			preparedStatement.setString( 7, leave.getStatus() );
			preparedStatement.setString( 8, leave.getType() );
			
			
			
			preparedStatement.execute();
			connection.commit();
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			System.out.print(e);
		}	finally {
			/*
			 * Close prepared statement and database connectivity at the end of
			 * transaction
			 */
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.print(e);
			}
		}

	}
//--------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public ArrayList<Leave> getLeavesList(int EmpId) {
		ArrayList<Leave> arrayList = new ArrayList<>();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_VIEW_ALL_LEAVES_USER));
			preparedStatement.setInt( 1, EmpId);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
				Leave leave = new Leave();
				leave.setLeaveId( resultSet.getInt(1) );
				leave.setEmpId( resultSet.getInt(2) );
				leave.setDepNo( resultSet.getInt(3) );
				leave.setStartDate( resultSet.getString(4) );
				leave.setEndDate( resultSet.getString(5) );
				leave.setCount( resultSet.getInt(6) );
				leave.setDescription( resultSet.getString(7) );
				leave.setStatus( resultSet.getString(8) );
				leave.setType( resultSet.getString(9) );
				
				leave.setFeedback( resultSet.getString(10) );
				
				arrayList.add(leave);
			}
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		return arrayList;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	@Override
	public ArrayList<Leave> getLeavesList(String Action) {
		ArrayList<Leave> arrayList = new ArrayList<>();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_VIEW_ALL_LEAVES_ADMIN));
		 if( Action.equalsIgnoreCase("Pending"))
		 {
			 preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_VIEW_ALL_PENDING_LEAVES_ADMIN_));
		 }
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
				Leave leave = new Leave();
				leave.setLeaveId( resultSet.getInt(1) );
				leave.setEmpId( resultSet.getInt(2) );
				leave.setDepNo( resultSet.getInt(3) );
				leave.setStartDate( resultSet.getString(4) );
				leave.setEndDate( resultSet.getString(5) );
				leave.setCount( resultSet.getInt(6) );
				leave.setDescription( resultSet.getString(7) );
				leave.setStatus( resultSet.getString(8) );
				leave.setType( resultSet.getString(9) );

				leave.setFeedback( resultSet.getString(10) );
				
				arrayList.add(leave);
			}
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		return arrayList;
	}
//--------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public void manageLeave( Leave leave) {
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_MANAGE_LEAVE));
			connection.setAutoCommit(false);
			preparedStatement.setString( 1 , leave.getStatus() );
			preparedStatement.setString( 2, leave.getFeedback() );
			preparedStatement.setInt( 3,  leave.getLeaveId() );
			System.out.println(preparedStatement);
			preparedStatement.execute();
			connection.commit();
			
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			System.out.println(e);
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		
	}
	
	
	
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	
	
	
	@Override

	public Leave getavailableLeaves(double EmpId )
	{
		Leave leave = new Leave();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_AVAILABLE_LEAVES));
			preparedStatement.setDouble( 1, EmpId);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
			
				
				
				leave.setCasualLeaves(resultSet.getInt(3));
				leave.setSickLeaves(resultSet.getInt(4));
				leave.setRequestLeaves(resultSet.getInt(5));
				
				
				
//			System.out.println("casula leaves  " +resultSet.getInt(3));
//			System.out.println("Sick leaves  " +resultSet.getInt(4));
//			System.out.println("Request leaves  " +resultSet.getInt(5));
				
				
			}
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		
		return leave;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------

	
	
	
	
	public void  addHolidays(ArrayList<Leave> leaves) {
	try {
		connection = DBConnectionUtil.getDBConnection();
		preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_ADD_HOLIDAYS));
		
	
		connection.setAutoCommit(false);
		
		for( Leave leave : leaves ) {
		preparedStatement.setString( 1, leave.getHolidayDate());
		preparedStatement.setString( 2, leave.getHolidayType() );
		
		preparedStatement.addBatch();
		}
	
		preparedStatement.executeBatch();
		connection.commit();
	
		
	} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
		System.out.print(e);
	}	finally {
		
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			System.out.print(e);
		}
	}

}
	
	
	
	
	
	
	
	
	//--------------------------------------------------------------------------------------------------------------------------------------

	public ArrayList<Leave> getHolidays(String Action) {
		ArrayList<Leave> arrayList = new ArrayList<>();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_VIEW_HOLIDAYS));
	
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
				Leave leave = new Leave();
				leave.setHolidayId( resultSet.getInt(1) );
				leave.setHolidayDate(resultSet.getString(2) );
				leave.setHolidayType(resultSet.getString(3) );
			

				
				arrayList.add(leave);
			}
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		return arrayList;
	
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	public ArrayList<Leave> getHolydates(String Action) {
		ArrayList<Leave> arrayList = new ArrayList<>();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.ID_VIEW_HOLIDAYS));
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
				Leave leave = new Leave();
				
				leave.setHolidayDate(resultSet.getString(2) );
				
				
				
				
				arrayList.add(leave);
			}
			
//			System.out.println("Holidyas  *                    *                 **********");
//			System.out.println(arrayList);
//			System.out.println("Holidyas  *                    *                 **********");
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {
				
				if( preparedStatement != null )
					preparedStatement.close();
				
				if(connection != null )
					connection.close();
				
			} catch (SQLException e) {
				
			}
		}
		return arrayList;
		
	}
	@Override
	public Leave getL(int Empid) {
		Leave leave = new Leave();
		try {
			connection = DBConnectionUtil.getDBConnection();
			preparedStatement = connection.prepareStatement(QueryUtil.queryByID(CommonConstants.GET_A_LEAVES));
			preparedStatement.setDouble( 1, Empid);
			preparedStatement.setDouble( 2, Empid);
			preparedStatement.setDouble( 3, Empid);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() )
			{
			
				
				
				leave.setCasualLeaves(resultSet.getInt(2));
				leave.setSickLeaves(resultSet.getInt(3));
				leave.setRequestLeaves(resultSet.getInt(1));
				
			}
			
		} catch (SQLException | SAXException | IOException | ParserConfigurationException | ClassNotFoundException e) {
			
		} finally {
			try {

			if( preparedStatement != null )
					preparedStatement.close();

			if(connection != null )
			connection.close();
			
				} catch (SQLException e) {
				
				}
		}
		
		return leave;
		
	}
	
	

	
	
	
}
